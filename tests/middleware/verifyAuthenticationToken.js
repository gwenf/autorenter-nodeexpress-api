const sinon = require('sinon');

describe('verifyAuthenticationToken Tests:', function() {
  it('should return 401 when token is missing', function() {
    const verifyAuthenticationToken = require('../../server/middleware/verifyAuthenticationToken');

    const request = {
      headers: {}
    };
    const response = {
      send: sinon.spy()
    };
    const status = () => {
      return response;
    };
    response.status = status;
    const statusSpy = sinon.spy(response, 'status');
    const nextSpy = sinon.spy();

    verifyAuthenticationToken(request, response, nextSpy);

    statusSpy.calledWith(401)
      .should.equal(true, 'Bad Status ' + response.status.args[0][0]);
    response.send.calledWith('Access token is missing')
      .should.equal(true, 'Invalid message "' + response.send.args[0][0] + '"');
    nextSpy.called.should.equal(false);
  });

  it('should return 401 when token is invalid', function() {
    const verifyAuthenticationToken = require('../../server/middleware/verifyAuthenticationToken');

    const request = {
      headers: {
        authorization: 'badtoken'
      }
    };
    const response = {
      send: sinon.spy()
    };
    const status = () => {
      return response;
    };
    response.status = status;
    const statusSpy = sinon.spy(response, 'status');
    const nextSpy = sinon.spy();

    verifyAuthenticationToken(request, response, nextSpy);

    statusSpy.calledWith(401)
      .should.equal(true, 'Bad Status ' + response.status.args[0][0]);
    response.send.calledWith('Access token is invalid')
      .should.equal(true, 'Invalid message "' + response.send.args[0][0] + '"');
    nextSpy.called.should.equal(false);
  });

  it('should call next when token is valid', function() {
    const verifyAuthenticationToken = require('../../server/middleware/verifyAuthenticationToken');
    const createAuthenticationToken = require('../../server/middleware/createAuthenticationToken');

    const token = createAuthenticationToken({
      username: 'testuser',
      id: 'b4349d14-1cd6-48f2-972e-aefd1ec88c78'
    });

    const request = {
      headers: {
        authorization: token
      }
    };
    const response = {
      send: sinon.spy()
    };
    const status = () => {
      return response;
    };
    response.status = status;
    const statusSpy = sinon.spy(response, 'status');
    const nextSpy = sinon.spy();

    verifyAuthenticationToken(request, response, nextSpy);

    nextSpy.called.should.equal(true);
    statusSpy.called.should.equal(false);
    response.send.called.should.equal(false);
  });
});
