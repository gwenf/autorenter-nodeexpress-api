const chai = require('chai');
const should = chai.should();

describe('createAuthenticationToken Tests:', function() {
  it('should return null when user is null', function() {
    const createAuthenticationToken = require('../../server/middleware/createAuthenticationToken');

    should.not.exist(createAuthenticationToken(undefined));
    should.not.exist(createAuthenticationToken(null));
  });

  it('should return token', function() {
    const createAuthenticationToken = require('../../server/middleware/createAuthenticationToken');

    const user = {
      id: '95da18d2-1968-476e-bab8-799598c9962e',
      username: 'melissajones'
    };

    const result = createAuthenticationToken(user);

    result.should.not.equal(null);
    result.should.be.a('string');
  });
});
