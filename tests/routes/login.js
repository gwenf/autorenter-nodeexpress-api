'use strict';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../server/app');
let expect = chai.expect;

chai.use(chaiHttp);

describe('routes/login', function() {
  describe('post', function() {
    it('should return a user', function() {
      const request = {
        'username': 'bobsmith',
        'password': 'secret'
      };

      return chai.request(server)
        .post('/api/login')
        .send(request)
        .then((response) => {
          expect(response).to.have.status(200);
          expect(response.body.username).to.be.eql('bobsmith');
          expect(response.body.token).to.be.a('string');
        })
        .catch((err) => {
          throw err;
        });
    });

    it('should NOT return a user', function() {
      const request = {
        'username': 'bobsmith',
        'password': 'bad_password'
      };

      return chai.request(server)
        .post('/api/login')
        .send(request)
        .then((response) => {
        })
        .catch((error) => {
          expect(error).to.have.status(401);
          expect(error.message).to.equal('Unauthorized');
        });
    });
  });
});
