'use strict';

const chai = require('chai');
const chaiHttp = require('chai-http');
const sinon = require('sinon');
const server = require('../../server/app');
const expect = chai.expect;

const config = require('../../server/config');
const createAuthenticationToken = require('../../server/middleware/createAuthenticationToken');
const token = createAuthenticationToken(
  {
    id: '95da18d2-1968-476e-bab8-799598c9962e',
    username: 'melissajones'
  }
);

const logDetail = require('../../server/services/logDetail');

chai.use(chaiHttp);

describe('routes/log', () => {
  describe('postLog', () => {
    const message = 'this is the message';
    let executeSpy;
    beforeEach(() => {
      executeSpy = sinon.spy(logDetail, 'execute');
    });

    it('should return 201', () => {
      return chai.request(server)
        .post('/api/log')
        .set('authorization', token)
        .set('content-type', 'application/json;charset=utf-8')
        .send({message: message})
        .then((res) => {
          expect(res).to.have.status(201);
        })
        .catch((err) => {
          throw err;
        });
    });

    it('should use default level', () => {
      const expectedArgs = [
        config.server.loggerLevel,
        message
      ];
      return chai.request(server)
        .post('/api/log')
        .set('authorization', token)
        .set('content-type', 'application/json;charset=utf-8')
        .send({message: message})
        .then((res) => {
          let actualArgs = executeSpy.args[0];
          expect(actualArgs).to.eql(expectedArgs);
        })
        .catch((err) => {
          throw err;
        });
    });

    it('should use provided level', () => {
      const level = 'error';
      const expectedArgs = [
        level,
        message
      ];
      return chai.request(server)
        .post('/api/log')
        .set('authorization', token)
        .set('content-type', 'application/json;charset=utf-8')
        .send({message: message, level: level})
        .then((res) => {
          let actualArgs = executeSpy.args[0];
          expect(actualArgs).to.eql(expectedArgs);
        })
        .catch((err) => {
          throw err;
        });
    });

    afterEach(() => {
      executeSpy.restore();
    });
  });
});
