﻿'use strict';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../server/app');
let expect = chai.expect;

const createAuthenticationToken = require('../../server/middleware/createAuthenticationToken');
const token = createAuthenticationToken(
  {
    id: '95da18d2-1968-476e-bab8-799598c9962e',
    username: 'melissajones'
  }
);

chai.use(chaiHttp);

describe('routes/skus', () => {
  describe('getSkus', () => {
    it('should pull the json data from the service', () => {
      let expectedOutput = {
        skus: [
          {makeId: 'tsl', modelId: 'tms', year: 2016, color: 'Black'},
          {makeId: 'tsl', modelId: 'tmx', year: 2016, color: 'Black'},
          {makeId: 'tsl', modelId: 'tms', year: 2017, color: 'Black'},
          {makeId: 'tsl', modelId: 'tms', year: 2017, color: 'Silver'},
          {makeId: 'tsl', modelId: 'tmx', year: 2017, color: 'Black'},
          {makeId: 'tsl', modelId: 'tmx', year: 2017, color: 'Silver'},
          {makeId: 'che', modelId: 'cvt', year: 2016, color: 'Black'},
          {makeId: 'che', modelId: 'cvt', year: 2016, color: 'Red'},
          {makeId: 'che', modelId: 'cvt', year: 2017, color: 'Black'},
          {makeId: 'che', modelId: 'cvt', year: 2017, color: 'Red'},
          {makeId: 'frd', modelId: 'fxp', year: 2016, color: 'Black'},
          {makeId: 'frd', modelId: 'fta', year: 2016, color: 'Black'},
          {makeId: 'frd', modelId: 'fta', year: 2016, color: 'Red'},
          {makeId: 'frd', modelId: 'fta', year: 2016, color: 'Silver'},
          {makeId: 'frd', modelId: 'fxp', year: 2017, color: 'Black'},
          {makeId: 'frd', modelId: 'fxp', year: 2017, color: 'Silver'},
          {makeId: 'frd', modelId: 'fta', year: 2017, color: 'Black'},
          {makeId: 'frd', modelId: 'fta', year: 2017, color: 'Red'},
          {makeId: 'frd', modelId: 'fta', year: 2017, color: 'Silver'}
        ]
      };

      return chai.request(server)
        .get('/api/skus')
        .set('authorization', token)
        .then((res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.be.eql(expectedOutput);
        })
        .catch((err) => {
          throw err;
        });
    });
  });
});
