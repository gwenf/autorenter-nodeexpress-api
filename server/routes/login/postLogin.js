'use strict';

module.exports = postLogin;

const loginService = require('../../services/loginService');

function postLogin(request, response, next) {
  loginService.login(request.body.username, request.body.password)
    .then((user) => {
      response.status(200);
      response.send(user);
    })
    .catch(next);
}
