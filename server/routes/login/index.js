'use strict';

const express = require('express');
const router = new express.Router();

module.exports = router;

/**
 * @swagger
 *
 * definitions:
 *   AuthCredentials:
 *     required:
 *       - username
 *       - password
 *     properties:
 *       username:
 *         type: string
 *       password:
 *         type: string
 *
 * /login:
 *   post:
 *     tags:
 *       - Login
 *     description: An endpoint to authenticate the User with basic username/password.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: auth
 *         description: "Authentication credentials in JSON format"
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/AuthCredentials'
 *     responses:
 *       200:
 *         description: Successfully authenticated
 */
router.post('/login', require('./postLogin'));
