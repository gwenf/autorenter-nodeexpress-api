'use strict';

const app = require('./app');
const http = require('http');
const config = require('./config');
const logger = require('./services/logger');

const port = config.server.port;
app.set('port', port);
logger.info(
  `Starting AutoRenter Node-Express API on port ${port}; environment=${config.server.environment}`);

const server = http.createServer(app);
server.listen(port);

process.on('uncaughtException', (err) => {
  logger.error(err);
});
