const userServiceFactory = require('./userServiceFactory');
const userService = userServiceFactory.build();

module.exports = userService;
