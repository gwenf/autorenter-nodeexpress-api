const loginServiceFactory = require('./loginServiceFactory');
const bcrypt = require('bcrypt');
const userService = require('./userService');
const createAuthenticationToken = require('../middleware/createAuthenticationToken');
const loginService = loginServiceFactory.build(bcrypt, userService, createAuthenticationToken);

module.exports = loginService;
