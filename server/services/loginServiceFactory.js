'use strict';

const errorTypes = require('../models/errorTypes');

function build(bcrypt, userService, createAuthenticationToken) {
  function login(username, password) {
    return new Promise(
      (resolve, reject) => {
        const user = userService.getUserByUsername(username);
        if (user !== undefined && bcrypt.compareSync(password, user.password)) {
          user.token = createAuthenticationToken(user);
          resolve(user);
        }
        reject({
          message: `Login failed for username '${username}'.`,
          errorType: errorTypes.loginFailed
        });
      }
    );
  }

  return {
    login: login
  };
}

module.exports = {build};
