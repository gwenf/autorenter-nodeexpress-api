'use strict';

const jsonwebtoken = require('jsonwebtoken');

function createAuthenticationToken(user) {
  if (user == null) return null;
  const token = {
    iss: process.env.JWT_ISSUER,
    aud: process.env.JWT_AUDIENCE,
    username: user.username,
    sub: user.id,
    iat: Math.floor(Date.now() / 1000),
    nbf: Math.floor(Date.now() / 1000),
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * process.env.JWT_DURATION_IN_HOURS)
  };
  return jsonwebtoken.sign(token, process.env.JWT_PASSWORD);
}

module.exports = createAuthenticationToken;
