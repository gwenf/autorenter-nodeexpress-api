'use strict';

const jsonwebtoken = require('jsonwebtoken');

function verifyAuthenticationToken(request, response, next) {
  if (process.env.DISABLE_AUTHORIZATION == 'true') {
    console.log('auth is off'); // eslint-disable-line no-console
    next();
    return;
  }
  const token = request.headers['authorization'];
  if (token) {
    try {
      const decodedToken = jsonwebtoken.verify(token, process.env.JWT_PASSWORD);
      if (decodedToken.exp <= Date.now() / 1000) {
        response.status(401).send('Access token has expired');
      }
      next();
    } catch (err) {
      response.status(401).send('Access token is invalid');
    }
  } else {
    response.status(401).send('Access token is missing');
  }
}

module.exports = verifyAuthenticationToken;
