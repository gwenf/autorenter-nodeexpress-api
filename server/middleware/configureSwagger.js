'use strict';

const config = require('../config');
const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');

function configureSwagger(app) {
  const swaggerHost = `${config.server.host}:${config.server.port}`;
  const swaggerDescription = `${config.server.description}<br><br>
      For most calls, you must be authorized to use the built-in curl requests, 
      i.e. the 'Try it out!' buttons. <br>
      To do that, use the Login endpoint below, copy the token value, click the authorize button 
      above and enter the token value as the API key. <br>
        All additional requests will then have the required authorization header. <br><br>
      The project readme is [here](http://${swaggerHost}/docs/dev/) <br>
      This API is intended to implement this 
      [spec](https://fusion-alliance.gitbooks.io/autorenter-spec/content/api/API_doc.html).`;

  const swaggerDefinition = {
    info: {
      title: config.server.title,
      version: config.server.version,
      description: swaggerDescription
    },
    host: swaggerHost,
    basePath: config.server.apiPrefix
  };

  const swaggerOptions = {
    swaggerDefinition: swaggerDefinition,
    apis: ['server/routes/**/index.js', 'server/routes/index.js']
  };

  const swaggerSpec = swaggerJSDoc(swaggerOptions);
  app.get('/swagger.json', function(request, response) {
    response.json(swaggerSpec);
  });

  const options = {
    validatorUrl: config.server.environment === 'production' ? 'https://autorenter-nodeexpress-api.herokuapp.com/swagger.json' : null //eslint-disable-line
  };

  app.use('/docs/api', swaggerUi.serve, swaggerUi.setup(swaggerSpec, true, options));
  app.use('/docs/dev', express.static('jsdoc/'));
}

module.exports = configureSwagger;
