'use strict';

const cors = require('cors');

function configureCors(app) {
  app.use(cors({
    origin: [
      'http://127.0.0.1:3000',
      'http://127.0.0.1:8080',
      'http://localhost:3000',
      'http://localhost:8080',
      'http://localhost:4200',
      'https://autorenter-angular1.herokuapp.com',
      'https://autorenter-nodeexpress-api.herokuapp.com'
    ],
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    preflightContinue: true
  }));
}

module.exports = configureCors;
