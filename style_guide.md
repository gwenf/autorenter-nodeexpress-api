# AutoRenter Style Guide - Node

## Naming

### Test file naming

* The name of the test file should be identical to the name of the test target's file.
	* The test file is distinguished from the target file by it's folder.
	
## Error Handling

Unless you need to do something specific to handle the error, just let the error propagate up the stack to the route handler.

Route handlers should handle errors by forwarding them on to the middleware. This is done by calling `next(error)`, as explained in [Proper Error Handling In ExpressJS Route Handlers](https://derickbailey.com/2014/09/06/proper-error-handling-in-expressjs-route-handlers/). 